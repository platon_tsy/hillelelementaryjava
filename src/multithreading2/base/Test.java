package multithreading2.base;

public class Test {

    public static void main(String[] args) {
        Object sync = new Object();
        int i = 0;

        synchronized (sync) {
            i++;
        }
    }

    public synchronized void someMethod() {

    }

    // Equal to above method only for non statics methods
    public void someMethod2(){
        synchronized(this){
            // code
        }
    }

}
