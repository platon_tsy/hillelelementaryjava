package multithreading2.base;

public class SomeClass {

    public static synchronized void someMethod(){
        //code
    }

    // Equal to above method only
    public static void someMethod2(){
        synchronized(SomeClass.class){
            //code
        }
    }

}
