package multithreading2.base;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTest {

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date);

        System.out.println(date.after(new Date(date.getTime() + 32423423)));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
        System.out.println(dateFormat.format(date));

        String receivedDate = "21 09 2018";

        try {
            Date parsedDate = dateFormat.parse(receivedDate);
            System.out.println(parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
