package io;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class FileExample {
    public static void main(String[] args) {
        File file = new File("test.txt");
        System.out.println(file.exists());
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(file.exists());
        System.out.println(file.getName());
        System.out.println(file.getParent());
        System.out.println(file.getAbsolutePath());
        try {
            System.out.println(file.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(file.length());

        File ioDirectory = new File("/home/platon/courses/Hillel Java OK/src/io");

        System.out.println(ioDirectory.isDirectory());
        System.out.println(file.isFile());

        System.out.println(java.util.Arrays.toString(ioDirectory.list()));

        FilenameFilter onlyJavaFiles = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                System.out.println(dir.getAbsolutePath());
                return name.endsWith(".java");
            }
        };

        if(ioDirectory.isDirectory()) {
            String[] javaFileNames = ioDirectory.list(onlyJavaFiles);
            System.out.println(java.util.Arrays.toString(javaFileNames));
        }

        System.out.println("Is file renamed ? = " + file.renameTo(new File("test2.txt")));
//        System.out.println("Is file deleted? = " + file.delete());
//        System.out.println(fil);
        File newDir = new File("newDir");
        newDir.mkdir();

        File newDirs = new File("newDirs/dir1/dir3");
        newDirs.mkdirs();


    }
}
