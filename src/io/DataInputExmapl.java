package io;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class DataInputExmapl {
    public static void main(String[] args) {
        try (DataInputStream input = new DataInputStream(new ByteArrayInputStream("234".getBytes()))) {
            System.out.println(input.readLong());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
