package io;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamExample {
    public static void main(String[] args) throws IOException {
        int bufferSize = 1024;
//        char[] buffer = new char[bufferSize];
        byte[] buffer = new byte[bufferSize];
        StringBuilder builder = new StringBuilder();
//        Reader in = new InputStreamReader(new FileInputStream("test2.txt"), "UTF-8");
        InputStream in = new FileInputStream("test2.txt");
        for( ; ; ) {
            int readSize = in.read(buffer, 0, buffer.length);
            if(readSize < 0) {
                break;
            }
            char c;
            for (int i = 0; i < readSize; i++) {
                c = (char) buffer[i];
                builder.append(c);
            }
        }

        System.out.println(builder.toString());
    }



}
