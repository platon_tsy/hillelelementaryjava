package io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedWriterExample {

    public static void main(String[] args) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("test2.txt", true))){
            writer.newLine();
            writer.append("Hello LAST!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
