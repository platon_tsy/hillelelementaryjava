package pattern.bridge;

public interface Color {
    String fill();
}
