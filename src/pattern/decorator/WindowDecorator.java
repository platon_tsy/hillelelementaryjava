package pattern.decorator;

public abstract class  WindowDecorator implements Window {

    protected Window widnowToBeDecorated;

    public WindowDecorator(Window widnowToBeDecorated) {
        this.widnowToBeDecorated = widnowToBeDecorated;
    }

    @Override
    public void draw() {
        //delegate
        this.widnowToBeDecorated.draw();
    }

    @Override
    public String getDescription() {
        //delegate
        return this.widnowToBeDecorated.getDescription();
    }
}
