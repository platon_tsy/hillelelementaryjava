package pattern.decorator;

public interface Window {

    void draw();
    String getDescription();

}
