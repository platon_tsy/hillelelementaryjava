package pattern.decorator;

public class HorizontalScrollBarDecorator extends WindowDecorator {
    public HorizontalScrollBarDecorator(Window widnowToBeDecorated) {
        super(widnowToBeDecorated);
    }

    @Override
    public void draw() {
        super.draw();
        drawHorizontalScroll();
    }

    private void drawHorizontalScroll() {

    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", including horizontal scrollbar";
    }
}
