package pattern.decorator;

public class VerticalScrollBarDecorator extends WindowDecorator {

    public VerticalScrollBarDecorator(Window widnowToBeDecorated) {
        super(widnowToBeDecorated);
    }

    @Override
    public void draw() {
        super.draw();
        //vertical scrollbar draw
        this.drawScrollBar();
    }

    private  void drawScrollBar() {

    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", including vertical scrollbar";
    }
}
