package pattern.singleton;

/*
+ Ленивая инициализация
+ Высокая производительность
- Невозможно использовать для не статических полей класса
 */
public class Singleton5 {
    private static class SingletonHolder {
        public static final Singleton5 HOLDER_INSTANCE = new Singleton5();
    }

    public static Singleton5 getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }
}
