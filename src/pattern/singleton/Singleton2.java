package pattern.singleton;

/*
+ Остроумно
+ Сериализация из коробки
+ Потокобезопасность из коробки
+ Возможность использования EnumSet, EnumMap и т.д.
+ Поддержка switch
- Не ленивая инициализация
 */
public enum Singleton2 {
    INSTANCE;
}
