package pattern.singleton;

/*
+ Простая и прозрачная реализация
+ Потокобезопасность
- Не ленивая инициализация
 */
public class Singleton1 {
    public static final Singleton1 INSTANCE = new Singleton1();
}
