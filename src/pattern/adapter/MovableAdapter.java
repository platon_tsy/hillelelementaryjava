package pattern.adapter;

public interface MovableAdapter {
    // returns speed in KMPH 
    double getSpeed();
}
