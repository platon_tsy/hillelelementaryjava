package pattern.adapter;

public interface Movable {
    // returns speed in MPH 
    double getSpeed();
}