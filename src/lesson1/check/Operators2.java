package lesson1.check;

/**
 * Created by platon on 20.07.17.
 */
public class Operators2 {
    public static void main(String[] args) {
        int x = 30;
        int y = 10;

        if( x == 30 ){
            if( y == 10 ){
                System.out.print("X = 30 и Y = 10");
            }
            System.out.print("X = 30");
        }



        //Тернарный оператор
        int z;

        if(x == 30) {
            z = 6;
        } else {
            z = 4;
        }
        System.out.println("z = " + z);
        z = x == 30 ? 6 : 4;
        System.out.println("z = " + z);
    }
}
