package exeption.advanced;

public class App20 {
    public static void main(String[] args) throws Throwable {
        try {
            Throwable t = new Exception(); // а лететь будет Exception
            throw t;
        } catch (Exception e) { // и мы перехватим Exception
            System.err.println("Перехвачено!");
        }
    }
}
