package exeption.advanced;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;

public class App13 {
    // пугаем ПРЕДКОМ исключений
    public static void main(String[] args) throws IOException {
        f0();
        f1();
    }
    public static void f0() throws EOFException { }
    public static void f1() throws FileNotFoundException { }
}
