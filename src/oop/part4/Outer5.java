package oop.part4;

//Anonymous Inner class
public class Outer5 {
    // Анонимный класс, который реализует интерфейс Hello
    static Hello h = new Hello() {
        public void show() {
            System.out.println("Метод внутреннего анонимного класса");
        }
    };

    public static void main(String[] args) {
//        h.show();

        Hello hello = new Hello() {
            @Override
            public void show() {
                System.out.println("Hi!");
            }
        };

        hello.show();
    }
}

interface Hello {
    void show();
}

class HelloImplementation implements Hello {

    @Override
    public void show() {
        System.out.println("Standart implementation");
    }
}
