package oop.part4;

//Method Local Inner class
public class Outer3 {
    void outerMethod() {
        int x1 = 50;
//        x1 = 10;
        System.out.println("Метод внешнего класса");
        // Внутренний класс является локальным для метода outerMethod()
        class Inner {
            int x = 10;
            public void innerMethod() {
                int x = 40;
                System.out.println("Метод внутреннего класса " + x1 + " " + x + " " + this.x);
            }
        }
        Inner inner = new Inner();
        inner.innerMethod();
    }

    // x should be final
    void outerMethod2() {
        int x = 98;
        System.out.println("Метод внешнего класса");
        class Inner {
            public void innerMethod() {
                System.out.println("x = " + x);
            }
        }
        Inner inner = new Inner();
        inner.innerMethod();
    }

    public static void main(String[] args) {
        Outer3 outer = new Outer3();
        outer.outerMethod();
        outer.outerMethod2();
    }
}

