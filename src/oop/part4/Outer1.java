package oop.part4;

//Nested class
public class Outer1 {
    private static int outerVal;
    // Статический внутренний класс
    static class Inner {
        private int outerVal;
        public void show() {
            System.out.println("Метод внутреннего класса" + this.outerVal + Outer1.outerVal);
        }
    }

    public static void main(String[] args) {
        Outer1.Inner inner = new Outer1.Inner();
        inner.show();
    }
}