package oop.part4;

public class Example {

    public static void main(String[] args) {
        show(new Hello2() {
            @Override
            public void show(String message) {
                System.out.println("Hi " + message + "!");
            }
        }, "people");

        show(new Hello2Impl(), "people");
    }


    static void show(Hello2 hello, String message) {
        hello.show(message);
    }
}

interface Hello2 {
    void show(String message);
}

class Hello2Impl implements Hello2 {

    @Override
    public void show(String message) {
        System.out.println("Go home " + message + " !");
    }
}
