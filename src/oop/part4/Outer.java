package oop.part4;

//Nested Inner class
public class Outer {
    private int outerVal;
    // Простой вложенный класс
    class Inner {
        public void show() {
            System.out.println("Inner method call" + outerVal);
        }
    }

    // не скомпилируется
//    class Inner {
//        static public void show() {
//            System.out.println("Метод внутреннего класса");
//        }
//    }


    public static void main(String[] args) {
        Outer.Inner inner = new Outer().new Inner();
        inner.show();
    }
}