package oop.part1;

/**
 * Created by platon on 07.08.17.
 */
public class Max {

    public static void main(String[] args) {
        int [] array = { 1, 5, 9, 3 };
        int [] array2 = { 13, 25, 59, 3 };
        int max = findMax(array);
        System.out.println(findMax(array2));
        System.out.println(max);
    }

    public static int findMax(int [] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if(max < arr[i]) {
                max = arr[i];
            }
        }
        return max;
    }

}
