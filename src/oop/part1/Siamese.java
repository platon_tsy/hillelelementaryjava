package oop.part1;

public class Siamese extends Cat {

    private String name;
    private Integer age;

    public String getName() {
        return name + " Siamese!";
    }

    public void setName(String name) {
        this.name = name;
        System.out.println(name);
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
