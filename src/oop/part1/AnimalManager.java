package oop.part1;

/**
 * Created by platon on 07.08.17.
 */
public class AnimalManager {
    public static void main(String[] args) {
        Animal animal = new Animal();
        System.out.println(animal.animalLegs());
        System.out.println(animal.whatItUseToBreeze);
//        System.out.println(animal.legs);
        animal.eyes = 5;
        System.out.println(animal.eyes);

//        Dog platonsDog = new Dog();
//        System.out.println(platonsDog.whatDoesDogEat);
//        System.out.println(platonsDog.whatDoesDogSay);
//        System.out.println(platonsDog.animalLegs());
//
//        Siamese platonsCat = new Siamese();
//        platonsCat.setAge(4);
//        platonsCat.setName("Robert");
//        platonsCat.setName("Robert2");
//        platonsCat.setName("Robert3");
//        System.out.println(platonsCat.getName());
//
        Siamese alexeysCat = new Siamese();
        alexeysCat.setName("Alex");
        alexeysCat.setAge(1);
//
//        System.out.println(alexeysCat.getName());

        Dog dog = new Dog();

        Animal animal1 = alexeysCat;
        Siamese convert = (Siamese) animal1;
        Sphinx petya = new Sphinx();
        Cat petya1 = petya;
//        System.out.println(convert.);
        Animal[] zoo = {
                alexeysCat,
                dog,
                petya
        };

        for (int i = 0; i < zoo.length; i++) {
            System.out.println(zoo[i].animalLegs());
            if(zoo[i] instanceof Cat) {
                Cat siamese = (Cat) zoo[i];
                System.out.println(siamese.getName());
            }
        }
    }

}
