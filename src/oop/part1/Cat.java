package oop.part1;

public class Cat extends Animal {

    public String whatDoesCatSay = "Meow!";
    public String whatDoesCatEat = "Fish";

    public String getWhatDoesCatSay() {
        return whatDoesCatSay + eyes;
    }

    private String name;

    public String getName() {
        return name;
    }
}
