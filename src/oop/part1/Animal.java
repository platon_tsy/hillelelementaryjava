package oop.part1;

//Родительский класс
public class Animal {

    private static int legs = 4;

    public static final String whatItUseToBreeze = "Air";

    protected int eyes;

    //Метод который выводит количество лап у птиц
    protected Integer animalLegs(){

        return this.legs;
    }

}
