package oop.task;

import oop.task.dep.Department;

import java.util.ArrayList;

public class Company {

    private String name;
    private ArrayList<Department> departments;

    public Company(String name, ArrayList<Department> departments) {
        this.name = name;
        this.departments = departments;
    }

    public int calculateSalary(int months) {
        int result = 0;
        for (Department department : departments) {
            result += department.calculateSalary(months);
        }
        System.out.println("Company " + this.name + "; salaries = " + result);
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(ArrayList<Department> departments) {
        this.departments = departments;
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", departments=" + departments +
                '}';
    }
}
