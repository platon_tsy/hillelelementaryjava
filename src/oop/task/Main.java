package oop.task;

import oop.task.dep.Department;
import oop.task.stuff.*;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Employee programmer = new Programmer("John", "Johnes", 3000);
        Employee tester = new Tester("Jack", "Daniels", 2800);
        Employee analyst = new Analyst("Sara", "Conor", 2900);

        Employee booker = new Booker("Marya", "Petrovna", 1500);
        Employee finDirector = new FinDirector("Boris", "Petrovich", 4000, 20);

        ArrayList<Employee> itEmployees = new ArrayList<>();
        itEmployees.add(programmer);
        itEmployees.add(tester);
        itEmployees.add(analyst);

        ArrayList<Employee> finEmployees = new ArrayList<>();
        finEmployees.add(booker);
        finEmployees.add(finDirector);

        Department itDepartment = new Department("IT", itEmployees);
        Department finDepartment = new Department("FIN", finEmployees);

//        List<Department> departments = Arrays.asList(itDepartment, finDepartment);
        ArrayList<Department> departments = new ArrayList<>();
        departments.add(itDepartment);
        departments.add(finDepartment);

        Company romashka = new Company("Romashka Company", departments);

        System.out.println(romashka);

//        itDepartment.calculateSalary(3);
//        finDepartment.calculateSalary(3);

        romashka.calculateSalary(7);

//        int [] a = {1,3,4};
//        System.out.println(max(4,9,10,2,5));
//        System.out.println(max(a));
     }


     public  static int max(int ... values) {
        int max = values[0];
         for (int i = 1; i < values.length; i++) {
             if(max < values[i]) {
                 max = values[i];
             }
         }
         return max;
     }

}
