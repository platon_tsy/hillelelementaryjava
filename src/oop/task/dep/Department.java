package oop.task.dep;

import oop.task.stuff.Employee;
import oop.task.stuff.FinDirector;

import java.util.ArrayList;

public class Department {

    private String name;
    private ArrayList<Employee> employees;

    public Department(String name, ArrayList<Employee> employees) {
        this.name = name;
        this.employees = employees;
    }

    public int calculateSalary(int months) {
        int result = 0;
        for (Employee employee : employees) {
            if(employee instanceof FinDirector) {
                result += calculateSalaryFor((FinDirector) employee, months);
            } else {
                result += employee.getSalary() * months;
            }
        }
        System.out.println("Department " + this.name + "; salaries = " + result);
        return result;
    }

    private int calculateSalaryFor(FinDirector finDirector, int months) {
        int result = 0;
        int countOfBonus = months / 3;
        int bonus = finDirector.getBonus();
        for (int i = 0; i < countOfBonus; i++) {
            int directorsSalary = bonus * finDirector.getSalary() / 100 + finDirector.getSalary();
            result += directorsSalary;
        }
        result += finDirector.getSalary() * (months - countOfBonus);
        return result;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Department{" +
                "name='" + name + '\'' +
                ", employees=" + employees +
                '}';
    }
}
