package oop.part2;


public class HTC extends Phone {

    @Override
    public void setModel(String model) {
        super.setModel(model);
    }

    @Override
    public void setMemory(Integer memory) {
        super.setMemory(memory);
    }

    @Override
    public String getOsType1() {
        return super.getOsType1();
    }

    @Override
    public String getOsType2() {
        return super.getOsType2();
    }

    @Override
    public String getMadeIn() {
        return super.getMadeIn();
    }

    @Override
    public String getModel() {
        return super.getModel();
    }

    @Override
    public Integer getMemory() {
        return super.getMemory();
    }

    //    private String model;
//    private Integer memory;
//    private Integer year;
//
//    public String getModel() {
//        return model;
//    }
//
//    public void setModel(String model) {
//        this.model = model;
//    }
//
//    public Integer getMemory() {
//        return memory;
//    }
//
//    public void setMemory(Integer memory) {
//        this.memory = memory;
//    }
//
//    public Integer getYear() {
//        return year;
//    }
//
//    public void setYear(Integer year) {
//        this.year = year;
//    }
//
//     public HTC() {
//    }
//
//    public HTC( String model, Integer memory, Integer year) {
//        this.model = model;
//        this.memory = memory;
//        this.year = year;
//    }
//
//    public String getModel() {
//        return model;
//    }
//
//    public Integer getMemory() {
//        return memory;
//    }
//
//    public Integer getYear() {
//        return year;
//    }
}
