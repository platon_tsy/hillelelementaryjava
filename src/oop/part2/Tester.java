package oop.part2;

/**
 * Created by platon on 10.08.17.
 */
public class Tester extends Employee {

    public Tester() {
    }

    public Tester(String name, String lastName, int age) {
        super(name, lastName, age);
    }

    @Override
    public String toString() {
        return "Tester{}" + super.toString();
    }

    public void action() {
        System.out.println("I'm very good tester!");
    }

    public int getSalary() {
        return 200 * 10;
    }
}
