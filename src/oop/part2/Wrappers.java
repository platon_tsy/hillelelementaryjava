package oop.part2;

import java.util.Arrays;

/**
 * Created by platon on 10.08.17.
 */
public class Wrappers {

    static String str;

    public static void main(String[] args) {
        Integer integer = new Integer(4);
        Integer int2 = 4;
        Integer int3 = null;

        int x = integer;
        Arrays.sort(new int[] {1,2,3});
        new Boolean(true);

//        str = 5;
//        str = Integer.toString(integer);
    }

}
