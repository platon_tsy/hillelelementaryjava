package oop.part2;

/**
 * Created by admin on 22.10.2016.
 */
public class IPhone extends Phone {
//    private String model;
//    private Integer memory;
//
//    public void setModel(String model) {
//        this.model = model;
//    }
//
//    public void setMemory(Integer memory) {
//        this.memory = memory;
//    }
//
//    public String getModel() {
//
//        return model;
//    }
//
//    public Integer getMemory() {
//        return memory;
//    }


    @Override
    public void setModel(String model) {
        super.setModel(model);
    }

    @Override
    public void setMemory(Integer memory) {
        super.setMemory(memory);
    }

    @Override
    public String getOsType1() {
        return super.getOsType1();
    }

    @Override
    public String getOsType2() {
        return super.getOsType2();
    }

    @Override
    public String getMadeIn() {
        return super.getMadeIn();
    }

    @Override
    public String getModel() {
        return super.getModel();
    }

    @Override
    public Integer getMemory() {
        return super.getMemory();
    }
}
