package oop.part2;


import java.util.ArrayList;

/**
 * Created by platon on 10.08.17.
 */
public class Main {
    public static void main(String[] args) {
        Employee programmerPetro = new Programmer();

        programmerPetro.setAge(35);
        programmerPetro.setName("Petro");
        programmerPetro.setLastName("Ivanov");


        Employee programmerOlya = new Programmer("Olya", "Unknown", 25);
        System.out.println(programmerOlya);
        System.out.println(programmerPetro);

        Tester testerVasily = new Tester("Vasiliy", "Pupkin", 50);
        System.out.println(testerVasily);

        programmerOlya.action();
        testerVasily.action();

        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(programmerOlya);
        employees.add(programmerPetro);
        employees.add(testerVasily);

        int sum = 0;
//        for (Employee e : employees) {
//            sum += e.getSalary();
//        }
        for (int i = 0; i < employees.size(); i++) {
            sum += employees.get(i).getSalary();
        }


        System.out.println("Sum = " + sum);
    }
}
