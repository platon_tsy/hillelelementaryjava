package oop.part2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by platon on 10.08.17.
 */
public class ArrayListOverview {
    public static void main(String[] args) {
        int [] arr = {1, 2, 3};
        int newArr [] = new int[arr.length + 1];
//        int big [] = new int[Integer.MAX_VALUE];
        System.arraycopy(arr, 0, newArr, 0, arr.length);
        newArr[newArr.length - 1] = 4;
        System.out.println(Arrays.toString(newArr));
        ArrayList<Integer> array = new ArrayList<>();
        System.out.println(array);
        array.add(2);
        array.add(3);
        array.add(4);
        array.add(4);
        System.out.println(array);
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Math.pow(2, 31));
//        array.remove(0);
        array.remove(new Integer(4));
        array.remove(new Integer(4));
        System.out.println(array.contains(4));
        System.out.println(array.indexOf(4));
        System.out.println(array);
        for (Integer myInt : array) {
            System.out.println(myInt);
        }
        array.size();
        System.out.println(array.get(1));
        array.set(1, 5);
        System.out.println(array);
    }
}
