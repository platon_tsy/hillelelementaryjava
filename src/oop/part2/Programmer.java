package oop.part2;

/**
 * Created by platon on 10.08.17.
 */
public class Programmer extends Employee {

    public Programmer () {
        System.out.println("Programmer created!");
    }

    public Programmer(String name, String lastName, int age) {
        super(name, lastName, age);
    }

    @Override
    public String toString() {
        return "Programmer{}" + super.toString();
    }

    public int getSalary() {
        return 200 * 15;
    }
}
