package oop.part3;

/**
 * Created by platon on 10.08.17.
 */
public abstract class Employee {


    public Employee(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        counter++;
    }

    private String name;
    private String lastName;
    private int age;
    public static int counter;
    public int counter2;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }

    public void action() {
        System.out.println("Give me my money!");
    }

    public int getSalary() {
        return 200;
    }

    public abstract void say(String message);

    public static void saySomething() {
        System.out.println("Hi!" + counter);

    }
}
