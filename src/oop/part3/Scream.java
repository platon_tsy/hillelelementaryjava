package oop.part3;

/**
 * Created by platon on 14.08.17.
 */
public class Scream {

    public void scream(String word) {
        System.out.println("scream " + word);
    }

    public void scream(String word, int count) {
        System.out.print("scream ");
        for (int i = 0; i < count; i++) {
            System.out.print(word);
        }
        System.out.println();
    }

    public void scream(String word1, String word2) {
        System.out.println(word1 + " " + word2);
    }

}
