package oop.part3;

/**
 * Created by platon on 14.08.17.
 */
public class Rectangle implements Shape {

    private int a;
    private int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculatePerimeter() {
        return (a + b) * 2;
    }

    @Override
    public double calculateSquare() {
        return a * b;
    }
}
