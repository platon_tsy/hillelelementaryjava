package oop.part3;

/**
 * Created by platon on 14.08.17.
 */
public class Circle implements Shape {

    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public double calculatePerimeter() {
        return Math.PI * 2 * radius;
    }

    @Override
    public double calculateSquare() {
        return Math.PI * radius * radius;
    }
}
