package oop.part3;

/**
 * Created by platon on 14.08.17.
 */
public class Animal {

    private String name;
    private String weight;
    private int countOfLegs;

    public Animal() {
    }

    public Animal(String name) {
        this.name = name;
    }

    public Animal(String name, String weight) {
        this(name);
        this.weight = weight;
    }

    public Animal(String name, String weight, int countOfLegs) {
        this(name, weight);
        this.countOfLegs = countOfLegs;
    }
}
