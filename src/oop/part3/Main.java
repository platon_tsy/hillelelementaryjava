package oop.part3;

import java.util.ArrayList;

/**
 * Created by platon on 14.08.17.
 */
public class Main {
    public static void main(String[] args) {
        Shape circle = new Circle(40);
        System.out.println("Perimeter of circle = " + circle.calculatePerimeter());

        Shape square = new Square(40);
        System.out.println("Perimeter of square = " + square.calculatePerimeter());

        Shape rectangle = new Rectangle(40, 60);
        System.out.println("Perimeter of rectangle = " + rectangle.calculatePerimeter());

        ArrayList<Shape> shapes = new ArrayList<>();
        shapes.add(circle);
        shapes.add(rectangle);
        shapes.add(square);

        int sum = 0;
        for (Shape shape : shapes) {
            sum += shape.calculatePerimeter();
            String name = getName(shape);
            System.out.println("Square " + name + " = " + shape.calculateSquare());
        }

        System.out.println("Sum of all shapes is = " + sum);

        //Перегрузка
        Scream scream = new Scream();
        scream.scream("Hi");
        scream.scream("Hello", 3);
        scream.scream("Hello", "!");
        System.out.println("");

        Animal animal = new Animal("dfsfd");
    }

    private static String getName(Shape shape) {
        String name = "";
        if(shape instanceof Square) {
            name = "square";
        } else if (shape instanceof Rectangle) {
            name = "rectangle";
        } else if (shape instanceof Circle) {
            name = "circle";
        }
        return name;
    }
}
