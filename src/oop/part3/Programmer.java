package oop.part3;

/**
 * Created by platon on 10.08.17.
 */
public final class Programmer extends Employee {

    public final String position = "middle";

    public Programmer(String name, String lastName, int age)
    {
        super(name, lastName, age);
    }

    @Override
    public String toString() {
        return "Programmer{}" + super.toString();
    }

    public int getSalary() {
        return 200 * 15;
    }

    @Override
    public void say(String message) {
        System.out.println(message + " programmer said!");

    }
}
