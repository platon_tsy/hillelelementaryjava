package oop.part3;

/**
 * Created by platon on 14.08.17.
 */
public class Bird extends Animal implements Shape, Soundable {
    @Override
    public double calculateSquare() {
        return 0;
    }

    @Override
    public double calculatePerimeter() {
        return 0;
    }

    @Override
    public void makeSound() {

    }
}
