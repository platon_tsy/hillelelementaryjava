package oop.part3;

/**
 * Created by platon on 14.08.17.
 */
public class Square implements Shape {

    private int a;

    public Square(int a) {
        this.a = a;
    }

    @Override
    public double calculatePerimeter() {
        return a * 4;
    }

    @Override
    public double calculateSquare() {
        return a * a;
    }
}
