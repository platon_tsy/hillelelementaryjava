package oop.part3;

/**
 * Created by platon on 14.08.17.
 */
public interface Soundable {
    void makeSound();
}
