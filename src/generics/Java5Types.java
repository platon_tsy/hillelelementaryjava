package generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dtv on 29.09.2016.
 */
public class Java5Types {

    public static void main(String[] args) {
            //with JAVA 7
            List<String> list1 = new ArrayList<>();
            list1.add("abc");
//            list1.add(new Integer(5)); //здесь будет ошибка во время компиляции программы
//
//            for(String str : list1){
//                //здесь не нужно использовать приведение типов, следовательно не следует беспокоиться о ClassCastException
//            }

    }

}
