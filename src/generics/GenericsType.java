package generics;/*

E — элемент (широко используется в Java Collections Framework,
например, ArrayList, Set и т.д.)
К — ключ (используется в Map)
Т — Тип
В — значение (используется в Map)
S, U, V и т.д. — 2й, 3й, 4й… тип

 */

public class GenericsType<T> {

    private T t;

    public T get(){
        return this.t;
    }

    public void set(T t1){
        this.t=t1;
    }

    public static void main(String args[]){
        GenericsType<String> type = new GenericsType<>();
        type.set("Str"); //здесь все нормально

        GenericsType type1 = new GenericsType();
        type1.set("Str"); //и здесь все нормально
        type1.set(10); //здесь также все отлично, так как работает автоупаковка
    }
}
