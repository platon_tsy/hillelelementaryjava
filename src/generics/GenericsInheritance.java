package generics;

public class GenericsInheritance {

    public static void main(String[] args) {
        String str = "abc";
        Object obj = new Object();
        obj = str; // в данном случае все будет работать, так как String является наследником Object

        MyClass<String> myClass1 = new MyClass<String>();
        MyClass<Object> myClass2 = new MyClass<Object>();
//        myClass2 = myClass1; // не скомпилируется, так как MyClass<String> не является MyClass<Object>
        obj = myClass1; // MyClass<T> наследник Object
    }

    public static class MyClass<T>{}

}