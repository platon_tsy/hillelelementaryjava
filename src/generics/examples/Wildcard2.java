package generics.examples;

import java.util.List;

/**
 * Created by dtv on 29.09.2016.
 */
public class Wildcard2 {

    public static void main(String[] args) {
//        List<?> intList = new ArrayList<Integer>();
//        intList.add(new Integer(10));

//        List<?> numList = new ArrayList<Integer>();
//        numList = new ArrayList<String>();

//        List<? extends Number> numList = new ArrayList<Integer>();
//        numList = new ArrayList<String>();

//        List<? extends Number> numList = new ArrayList<Integer>();
//        numList = new ArrayList<Double>();

//        List<? super Integer> intList = new ArrayList<Integer>();
//        System.out.println("The intList is: " + intList);
    }

    public static Double sum(List<? extends Number> numList) {
        Double result = 0.0;
        for (Number num : numList) {
            result += num.doubleValue();
        }
        return result;
    }

}
