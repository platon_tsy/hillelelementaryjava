package generics.examples;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LinkedListGeneric {
    public static void main(String[] args) {
//        List list = new LinkedList();
//        list.add("First");
//        list.add("Second");
//        list.add(100000000);
//        List<String> list2 = list;
//        for(Iterator itemItr = list2.iterator(); itemItr.hasNext();) {
//            System.out.println(itemItr.next());
//        }

        //Generic type
        List<String> list = new LinkedList<String>();
        list.add("First");
        list.add("Second");
        List list2 = list;
        for(Iterator<String> itemItr = list2.iterator(); itemItr.hasNext();)
            System.out.println(itemItr.next());
    }
}
