package generics.examples;

import java.util.Arrays;
import java.util.List;

public class AnimalPrinter {

    public static void main(String[] args) {
        Animal abstractAnimal = new Animal();
        abstractAnimal.setName("Abstract");

        Bird bird = new Bird();
        bird.setName("Unnamed bird");

        Duck duck = new Duck();
        duck.setName("DUck");

        Cat cat = new Cat();
        cat.setName("Cat");

        BlackCat blackCat = new BlackCat();
        blackCat.setName("BlackCat");

        catPrinter(Arrays.asList(cat, blackCat));
        animalPrinter(Arrays.asList(cat, blackCat, duck));
        animalPrinter2(Arrays.asList(bird, abstractAnimal, duck, cat));
    }

    public static void catPrinter(List<? extends Cat> cat) {
        System.out.println(cat);
    }

    public static void animalPrinter(List<? extends Animal> animal) {
        System.out.println(animal);
    }

    public static void animalPrinter2(List<? super Bird> animal) {
        System.out.println(animal);
    }
}
