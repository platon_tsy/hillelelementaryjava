package generics.examples;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {
    public static <T> void fill(List<T> list, T val) {
        for (int i = 0; i < list.size(); i++) {
            list.set(i, val);
        }
    }

    public static void main(String[] args) {
        List<Integer> intList = new ArrayList<Integer>();
        intList.add(1);
        intList.add(2);
        System.out.println("Список до обработки дженерик-методом: " + intList);
        ListUtil.fill(intList, 0);
        System.out.println("Список после обработки дженерик-методом: "
                + intList);
    }
}
