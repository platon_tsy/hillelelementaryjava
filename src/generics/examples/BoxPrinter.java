package generics.examples;

//public class BoxPrinter {
//    private Object val;
//
//    public BoxPrinter(Object arg) {
//        val = arg;
//    }
//
//    public String toString() {
//        return "{" + val + "}";
//    }
//
//    public Object getValue() {
//        return val;
//    }
//
//
//}

public class BoxPrinter<T> {
    private T val;

    public BoxPrinter(T arg) {
        val = arg;
    }

    public String toString() {
        return "{" + val + "}";
    }

    public T getValue() {
        return val;
    }

    public static void main(String[] args) {
//        BoxPrinter value1 = new BoxPrinter(new Integer(10));
//        System.out.println(value1);
//        Integer intValue1 = (Integer) value1.getValue();
//
//        BoxPrinter value2 = new BoxPrinter("Hello world");
//        System.out.println(value2);
//
//        // Здесь программист допустил ошибку, присваивая
//        // переменной типа Integer значение типа String.
//        Integer intValue2 = (Integer) value2.getValue();

        //------------Generics

        BoxPrinter<Integer> value1 = new BoxPrinter<Integer>(10);
        System.out.println(value1);
        Integer intValue1 = value1.getValue();
        BoxPrinter<String> value2 = new BoxPrinter<String>("Hello world");
        System.out.println(value2);
//
//        // Здесь повторяется ошибка предыдущего фрагмента кода
//        Integer intValue2 = value2.getValue();
    }
}
