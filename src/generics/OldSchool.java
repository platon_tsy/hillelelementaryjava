package generics;

/**
 * Created by dtv on 29.09.2016.
 */
public class OldSchool {

    private Object t;

    public Object get() {
        return t;
    }

    public void set(Object t) {
        this.t = t;
    }

    public static void main(String args[]){
        OldSchool type = new OldSchool();
        type.set("Str");
        String str = (String) type.get(); //приведение типов может стать причиной ClassCastException
    }

}
