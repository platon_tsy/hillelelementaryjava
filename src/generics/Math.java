package generics;

public class Math <T extends Comparable> {

        public T max(T[] array){
            T max = array[0];
            for(T t : array){
                if(max.<T>compareTo(t) < 0){
                    max = t;
                }
            }
            return max;
        }


    public static void main(String[] args) {
        Math<Double> math = new Math<>();
        System.out.println(math.max(new Double[]{4.0, 15., 10.}));
    }

}
