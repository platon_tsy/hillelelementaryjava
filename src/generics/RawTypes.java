package generics;

import java.util.ArrayList;
import java.util.List;

/*
Genrics* были добавлены в Java 5 и сейчас являются неотъемлемой частью Java Core.
Если вы знакомы с Java Collections версии 5 или выше, то я уверен,
что вы использовали generics в своих программах.

*Дженерики/Родовые типы — далее я буду использовать название
* Generics или дженерики как наиболее используемые,
* хотя правильный перевод все-таки Родовые типы

Использовать дженерики с классами коллекций не только легко,
но и дает гораздо больше возможностей, чем просто задание типа коллекции.
 Чтобы процесс изучения дженериков был более простым и понятным,
 мы преимущественно будем использовать код и комментарии к нему.
*/
public class RawTypes {

    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("abc");
        list.add(5); //OK

        for(Object obj : list){
            String str = (String) obj; //здесь приведение типов бросит ClassCastException
        }
    }

}
