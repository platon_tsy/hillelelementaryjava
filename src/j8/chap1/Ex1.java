package j8.chap1;

public class Ex1 {

    public static void main(String[] args) {

        Runnable oldRunnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("It's old runnable");
                test();
            }

            public void test() {
                System.out.println("!!!!!!!!!");
            }
        };

        Runnable runnable = () -> { System.out.println("It's new style runnable!"); };
        Runnable runnable2 = () -> System.out.println("It's new style runnable!");

        Thread thread = new Thread(() -> System.out.println("Test"));
        thread.start();
    }

}
