package j8.chap4;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Sum {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 43, 564, 23, 45, 67, 32, 67, 9);
        System.out.println(sum(list));
        System.out.println(sumEven(list));
        System.out.println(sumMoreThan(list, 3));

        System.out.println(sumUniversal(list, (Integer value) -> { return  true;}));
        System.out.println(sumUniversal(list,  value -> true));

        System.out.println(sumUniversal(list, value -> value % 2 == 0));

        System.out.println(sumUniversal(list, value -> value > 3));
        System.out.println(sumUniversal(list, value -> value > 13));
    }


    public static int sum(List<Integer> list) {
        int sum = 0;
        for(Integer value : list) {
            sum += value;
        }
        return sum;
    }


    public static int sumEven(List<Integer> list) {
        int sum = 0;
        for(Integer value : list) {
            if(value % 2 == 0) {
                sum += value;
            }
        }
        return sum;
    }

    public static int sumMoreThan(List<Integer> list, int threashould) {
        int sum = 0;
        for(Integer value : list) {
            if(value > threashould) {
                sum += value;
            }
        }
        return sum;
    }


    public static int sumUniversal(List<Integer> list, Predicate<Integer> predicate) {
        int sum = 0;
        for(Integer value : list) {
            if(predicate.test(value)) {
                sum += value;
            }
        }
        return sum;
    }

}
