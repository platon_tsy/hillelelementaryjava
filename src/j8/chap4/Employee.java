package j8.chap4;

public class Employee {
	public String name;
	public int age;
	
	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + "]";
	}
	
	
}
