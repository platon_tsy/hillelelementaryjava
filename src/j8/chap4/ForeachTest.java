package j8.chap4;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class ForeachTest {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Petya", "Vasya", "Katya", "Dasha");

//        names.forEach(name -> System.out.println(name + "!"));
//        names.forEach(System.out::println);
//        names.forEach(ForeachTest::print);

        names
            .stream()
            .filter(name -> name.startsWith("P"))
            .forEach(System.out::println);

        List<String> pNames = names
                .stream()
                .filter(name -> name.startsWith("P"))
                .collect(toList());
        System.out.println(pNames);

        Employee employee1 = new Employee();
        Employee employee2 = new Employee();
        Employee employee3 = new Employee();
        Employee employee4 = new Employee();

        employee1.age = 20;
        employee1.name = "Petya";

        employee2.age = 24;
        employee2.name = "Dasha";

        employee3.age = 28;
        employee3.name = "Katya";

        employee4.age = 32;
        employee4.name = "Vasya";

        List<Employee> employees = Arrays.asList(employee1, employee2, employee3, employee4);

        employees
                .stream()
                .map(e -> e.age)
                .filter(age -> age > 25)
                .forEach((age) -> System.out.println(age + "!"));
    }

    public static void print(String s) {
        System.out.println(s + "!!!");
    }


}
