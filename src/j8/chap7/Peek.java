package j8.chap7;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;


public class Peek {

    public static void main(String[] args) {

        Stream<Integer> integerStream = Stream.of(2, 3, 4, 5);
        integerStream.forEach(i -> {
            int a = i * 2;
        });

        List<Integer> result = Stream.of(2, 3, 4, 5)
                .peek(Peek::test).map(x -> x + 17)
                .peek(x -> System.out.println("after map: " + x)).filter(x -> x % 2 == 0)
                .peek(x -> System.out.println("after filter: " + x)).limit(3)
                .peek(x -> System.out.println("after limit: " + x)).collect(toList());
    }

    public static void test (Integer x) {
         System.out.println("taking from stream: " + x);
    }
}
