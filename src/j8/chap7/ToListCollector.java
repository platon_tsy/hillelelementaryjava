package j8.chap7;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import static java.util.stream.Collector.Characteristics.CONCURRENT;
import static java.util.stream.Collector.Characteristics.IDENTITY_FINISH;

public class ToListCollector<T> implements Collector<T, List<T>, List<T>> {

    //Making a new result container: the supplier method
    @Override
    public Supplier<List<T>> supplier() {
        return () -> new ArrayList<T>();
    }

    //Adding an element to a result container: the accumulator method
    @Override
    public BiConsumer<List<T>, T> accumulator() {
        return (list, item) -> list.add(item);
    }

    //Applying the final transformation to the result container: the finisher
    //method
    //Question what we can do with this method? Function.identity();
    @Override
    public Function<List<T>, List<T>> finisher() {
        return Function.identity();
    }

    //Merging two result containers: the combiner method
    @Override
    public BinaryOperator<List<T>> combiner() {
        return (list1, list2) -> {
            list1.addAll(list2);
            return list1;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Collections.unmodifiableSet(EnumSet.of(IDENTITY_FINISH, CONCURRENT));
    }
}
