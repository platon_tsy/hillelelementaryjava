package j8.chap7;

import java.util.Arrays;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(1, 2, 3);
        List<Integer> collect = integers.stream().filter(v -> v % 2 == 0).collect(new ToListCollector<>());
        System.out.println(collect);

//        integers.stream()
//                .parallel()
//                .filter(v -> v % 2 == 0)
//                .sequential()
//                .map(fdsdf)
//                .parallel()
//                .collect(ArrayList::new, List::add, List::addAll);

        integers.addAll(integers);
    }

}
