package j8.chap6;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Question {

    public static void main(String[] args) {
        List<String> words = Arrays.asList("Hello", "world");
        words.stream()
                .map(word -> word.split(""))
                .distinct()
                .forEach(s -> System.out.println(Arrays.toString(s)));

        words.stream()
                .map(word -> word.split(""))
                .map(Arrays::stream)
                .distinct()
                .forEach(System.out::println);











        words.stream()
                .map(word -> word.split(""))
                .flatMap(Arrays::stream)
                .distinct()
                .forEach(System.out::println);

        //Get square of each number
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> squareNumbers = numbers.stream().map(value -> value * value).collect(toList());
        System.out.println(squareNumbers);

        //return only pairs whose sum is divisible by 3
        List<Integer> numbers1 = Arrays.asList(1, 2, 3);
        List<Integer> numbers2 = Arrays.asList(3, 4);

        List<int[]> pairs =
                numbers1.stream()
                        .flatMap(i -> numbers2.stream()
                                .filter(j -> (j + i) % 3 == 0)
                                .map(j -> new int[]{i, j})
                        )
                        .collect(toList());
        System.out.println(pairs.size());
        pairs.stream().forEach(el -> System.out.println(Arrays.toString(el)));
    }

}
