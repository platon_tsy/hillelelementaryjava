package j8.ftest;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Question1 {

    public static void main(String[] args) {
        List<String> str = Arrays.asList("a","b","A","B");
        //Write using method reference
        str.stream().map((String s) -> s.toUpperCase()).collect(toList());
        str.sort((s1, s2) -> s1.compareToIgnoreCase(s2));

        //How to get integers from strings using j8?
        List<String> numbers = Arrays.asList("1","2","3","4");
    }

}
