package j8.ftest;

@FunctionalInterface
public interface SimpleFuncInterface {
    void doWork();
    String toString();
    boolean equals(Object o);
}
