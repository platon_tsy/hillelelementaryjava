package j8.ftest;

public class Employee {
	public String name;
	public int age;

	public Employee() {
	}

	public Employee(String name) {
		this.name = name;
	}

	public Employee(int age) {
		this.age = age;
	}

	public Employee(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + "]";
	}
	
	
}
