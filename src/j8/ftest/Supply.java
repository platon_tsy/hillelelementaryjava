package j8.ftest;

import annotation.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class Supply {

    public static void main(String[] args) {
        //equivalent

        Supplier<Employee> supplier = Employee::new;
        Employee employee = supplier.get();

        Supplier<Employee> supplier1 = () -> new Employee();
        Employee employee1 = supplier1.get();


        Supplier<Employee> userFactory = () -> {
            Scanner in = new Scanner(System.in);
            System.out.println("Введите имя: ");
            String name = in.nextLine();
            return new Employee(name);
        };

        Employee user1 = userFactory.get();
        Employee user2 = userFactory.get();

        System.out.println("Имя user1: " + user1.name);
        System.out.println("Имя user2: " + user2.name);


        // How to create employees using method reference???
        Function<Integer, Employee> function = age -> new Employee(age);
        Function<Integer, Employee> function2 = Employee::new;
            function2.apply(25);

        BiFunction<String , Integer, Employee> biFunction =
                (String name, Integer age) -> new Employee(name, age);
        BiFunction<String, Integer, Employee> biFunction2 =
                Employee::new;
        biFunction2.apply("Petya", 30);

        //How to write method that will create new objects(employees) by each age
        List<Integer> ages = Arrays.asList(13, 24, 54, 65);

        createByAges(ages, function).stream().filter(e -> e.age <= 13).forEach(System.out::println);
        System.out.println(createByAges(ages, (age) -> new Person(age)));

        //How to instantinate constructor with three arguments?
    }

    public static<T> List<T> createByAges(List<Integer> list, Function<Integer, T> f) {
        List<T> result = new ArrayList<>();
        list.forEach(age -> result.add(f.apply(age)));
        return result;
    }

}
