package collections.part2;

import java.util.HashMap;
import java.util.Map;

public class HashMapOverview {

    public static void main(String[] args) {
        Map<String, Integer> hashMap = new HashMap<>(50, 0.9f);
        //loadfactor * capacity = threshold
        hashMap.put(null, 10);
        hashMap.put(null, 13);
        "petro".hashCode();
        hashMap.put("petro", 13);

        Map<Person, Integer> persons = new HashMap<>();
//        persons.put(new Person(1, "Petro", "Ivanov"), 10);
//        persons.put(new Person(2, "Petro", "Johnes"), 13);

        Address address = new Address("Khreshtik");

        Person person = new Person(1, "Petro", "Ivanov", address);
        Person person2 = new Person(2, "Petro", "Ivanov", address);

        persons.put(person, 20);
        persons.put(person2, 30);

        address.setA("Kirova");
        person.getAddress().setA("new street");

//        person.setFirstName("PEtro2");

        System.out.println(person.equals(person));

        for(Map.Entry<String, Integer> entry : hashMap.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }
    }

}
