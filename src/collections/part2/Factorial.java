package collections.part2;

public class Factorial {

    public static void main(String[] args) {
        System.out.println(factorial(5));
    }
//Recursion
    public static int factorial(int x) {
        if(x == 1) return 1;
        return x * factorial(x - 1);
    }

}
