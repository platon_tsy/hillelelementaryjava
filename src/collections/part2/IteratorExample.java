package collections.part2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class IteratorExample {

    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Pavel");
        names.add("Andrey");
        names.add("Sergey");
        names.add("Lubomir");
//        Arrays.asList("Pavel", "Andrey", "Sergey", "Lubomir");
        //Exception UnsupportedOperationException
//        for (int i = 0; i < names.size(); i++) {
//            if(names.get(i).equals("Andrey")) {
//                names.remove(i);
//            }
//        }
//
//        Iterator<String> namesIterator = names.iterator();
//        while (namesIterator.hasNext()) {
//            String name = namesIterator.next();
//            if(name.equals("Andrey")) {
//                namesIterator.remove();
//            }
//        }
//        int indexToRemove = -1;
//        for (int i = 0; i < names.size(); i++) {
//            if(names.get(i).equals("Pavel")) {
//                indexToRemove = i;
//            }
//        }
//
//        if(indexToRemove != -1) {
//            names.remove(indexToRemove);
//        }
//
//        System.out.println(names);

        LinkedList<String> linkedList = new LinkedList<>(names);
        System.out.println(linkedList);

        ListIterator<String> listIterator = linkedList.listIterator();
//        while (listIterator.hasNext()) {
//            if(listIterator.hasPrevious() && listIterator.nextIndex() == 2) {
//                System.out.println("Prev = " + listIterator.previous());
//            }
//            System.out.println("Next = " + listIterator.next());
//        }


        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
            System.out.println(listIterator.nextIndex());
            if(listIterator.nextIndex() == 3) {
                listIterator.add("New student");
            }
        }
        System.out.println(linkedList);
    }

}
