package collections.part2;

public class Main {
    public static void main(String[] args) {
        List list = new List();
        list.addBack(1);
        list.addBack(2);
        list.addBack(3);
        list.addFront(6);

        list.printList();
        System.out.println();

//        list.delEl(6);
//        list.delEl(5);
//        list.delEl(12);
//        list.delEl(2);
        ListElement element = list.get(2);
        System.out.println("List element #2 = " + element.data);
        list.printList();
        System.out.println();

        List emptyList = new List();
        ListElement emptyElem = emptyList.get(3);
//        System.out.println("List element #3 = " + emptyElem.data);
        String nullString = "";
        nullString.length();
    }
}
