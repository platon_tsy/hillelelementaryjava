package collections.part2;

public class Address {
    private String a;

    public Address(String a) {
        this.a = a;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }
}
