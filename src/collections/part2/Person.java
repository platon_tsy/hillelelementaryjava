package collections.part2;
//1) Рефлексивность: Объект должен равняться себе самому.
//2) Симметричность: если a.equals(b) возвращает true, то b.equals(a) должен тоже вернуть true.
//3) Транзитивность: если a.equals(b) возвращает true
// и b.equals© тоже возвращает true, то c.equals© тоже должен возвращать true.
//4) Согласованность: повторный вызов метода equals() должен возвращать одно и тоже значение до тех пор,
// пока какое-либо значение свойств объекта не будет изменено.
// То есть, если два объекта равны в Java, то они будут равны пока их свойства остаются неизменными.
//5) Сравнение null: объект должны быть проверен на null.
// Если объект равен null, то метод должен вернуть false, а не NullPointerException.
// Например, a.equals(null) должен вернуть false.
public final class Person {
    private int id;
    private String firstName;
    private String lastName;
    private Address address;

    public Person(int id, String firstName, String lastName, Address address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.address = new Address(address.getA());
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id;}

    public String getFirstName() { return firstName; }

    public String getLastName() {
        return lastName;
    }

    public Address getAddress() {
        return new Address(address.getA());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (id != person.id) return false;
        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        return lastName != null ? lastName.equals(person.lastName) : person.lastName == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }
}
