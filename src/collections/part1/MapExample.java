package collections.part1;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapExample {
    public static void main(String[] args) {
        Map<String, Integer> integerMap = new HashMap<>();
        integerMap.put("Ivan", 23);
        integerMap.put("Petro", 25);
        integerMap.put("Vasya", 45);
        integerMap.put("Olya", 40);
        integerMap.put("Olya", 46);
        integerMap.put(null, 46);

        System.out.println(integerMap.get("Olya"));
        System.out.println(integerMap.get(null));

        for( Map.Entry<String, Integer> entry : integerMap.entrySet()) {
            System.out.println("Key = " + entry.getKey());
            System.out.println("Value = " + entry.getValue());
        }

        System.out.println(integerMap.values());
        System.out.println(integerMap.keySet());

        if(!integerMap.containsKey("Vadim")) {
            integerMap.put("Vadim", 23);
        }

        integerMap.putIfAbsent("Vadim", 23);

        Map<String, String> treeMap = new TreeMap<>();
    }
}
