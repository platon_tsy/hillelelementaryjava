package enums;

public enum DAY {
    Monday("пн"),
    Teusday("вт"),
    Wednesday("ср"),
    Thirthday("чт"),
    Friday("пт"),
    Saturday("сб"),
    Sunday("вс");

    private String shortName;

    DAY(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
