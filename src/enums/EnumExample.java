package enums;

public class EnumExample {

    public enum Company  {
        EBAY, PAYPAL, GOOGLE, YAHOO
    }

    Company cName;

    public EnumExample(Company cName) {
        this.cName = cName;
    }

    public void companyDetails() {
        switch (cName) {
            case EBAY:
                System.out.println("Biggest Market Place in the World.");
                break;

            case PAYPAL:
                System.out.println("Simplest way to manage Money.");
                break;

            case GOOGLE:
            case YAHOO:
                System.out.println("1st Web 2.0 Company.");
                break;

            default:
                System.out.println("Google - biggest search giant.. ");
                break;
        }
    }

    public static void main(String[] args) {
        EnumExample ebay = new EnumExample(Company.EBAY);
        ebay.companyDetails();
        EnumExample paypal = new EnumExample(Company.PAYPAL);
        paypal.companyDetails();
        EnumExample google = new EnumExample(Company.GOOGLE);
        google.companyDetails();
        EnumExample yahoo = new EnumExample(Company.YAHOO);
        yahoo.companyDetails();

        DAY monday = DAY.Monday;
        DAY wednesday = DAY.Monday;
        if(monday == wednesday) {
            System.out.println("It's really monday! amazing!");
        }

        monday.setShortName("вт");
        System.out.println();
        System.out.println(monday.getShortName());
        System.out.println();
        for (DAY d : DAY.values()) {
            System.out.println(d.getShortName());
        }

        System.out.println(monday.ordinal());
        System.out.println(DAY.valueOf("Sunday").getShortName());
        System.out.println(DAY.Friday.compareTo(DAY.Monday));
    }
}

