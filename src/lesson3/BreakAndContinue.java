package lesson3;

public class BreakAndContinue {

    public static void main(String[] args) {

        //Оператор break
        int x[] = {2, 43, 3, 6, 7, 89};

        int result = 0;

        for (int i = 0; i < x.length ; i++) {
            if(i == 4) {
                break;
            }
            if (x[i] < 100){
                result += x[i];
            }
        }
        System.out.println(result);

            //Оператор continue
       for (int i = 0; i < x.length ; i++) {
            if (x[i] == 43){
                continue;
            }
            System.out.print(x[i] + " ");
        }

        System.out.println();

        for (int i = 0; i < x.length; i++) {
            if(x[i] == 6) {
                System.out.println("Ура! Нашли");
                break;
            }
            System.out.println("-----------");
        }

        System.out.println();

        for (int i = 0; i <x.length ; i++) {
            if (x[i] % 2 == 0){
                continue;
            }
            System.out.print(x[i] + " ");
        }
    }
}
