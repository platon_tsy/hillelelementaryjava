package lesson3;


public class Arrays {

    public static void main(String[] args) {

        int a1 = 5;
        int a2 = 8;

//        int tmp = a1;
//        a1 = a2;
//        a2 = tmp;

        a1 = a1 + a2;
        a2 = a1 - a2;
        a1 = a1 - a2;

        //Создаем массив
        int y [] = new int[3];
//        System.out.println(y[10]);
        int x [] = new int[5];
        int index = 0;
        x[index] = 3;
        x[1] = 6;
        x[2] = 10;
        x[3] = 12;
        x[4] = 5;


        // Перебираем массив
        System.out.print("Выводим все элементы нашего массива : ");

        for (int i = 0; i < x.length ; i++) {
            System.out.print("[" + x[i] + "]");
        }

        System.out.println();

       //Перебираем массив в обратном порядке
        System.out.print("Выводим все элементы нашего массива в обратном порядке : ");
        for (int i = x.length - 1; i >= 0 ; i--) {
            System.out.print("[" + x[i] + "]");
        }

        System.out.println();

       //Выводим все парные элементы массива
        System.out.print("Выводим все четные элементы массива : ");
        for (int i = 0; i < x.length; i++) {
            if (x[i] % 2 == 0) {
                System.out.print("[" + x[i] + "]");
            }
        }

        System.out.println();

        // Выведем сумму всех элемнтов массива
        System.out.print("Сумма всех элементов массива = ");
        int sum = 0;
        for (int i = 0; i < x.length ; i++) {
//            sum = sum + x[i];
            sum += x[i];
            System.out.println(sum);
        }


        //Создаем массив
       int x2 [] = new int[5];
        x2[0] = 3;
        x2[1] = 6;
        x2[2] = 9;
        x2[3] = 12;
        x2[4] = 90;

        System.out.println();

        // Выведем сумму первого и последнего элементов массива
        System.out.print("Сумма первого и последнего элементов массива = ");

        int sumFirstAndLast = 0;

        //Как оптимизировать этот код?
//        for (int i = 0; i < x2.length; i++) {
            sumFirstAndLast = x2[0] + x2[x2.length - 1];
//        }

        System.out.print(sumFirstAndLast);

        System.out.println();


//        // Алтернативный способ создать массив
//        int mass [] = {4, 0, 7, 1, 3};
//        System.out.println(mass);
//        System.out.println(java.util.Arrays.toString(mass));
//
//       // Длина массива
//        System.out.println("Длина массива = " + mass.length);
//
//       // Перебираем массив
//        System.out.print("Перебрали массив : " );
//        for (int i = 0; i < mass.length ; i++) {
//            System.out.print(mass[i] + " ");
//        }
//
//        System.out.println();



        // Алтернативный способ создать массив
        int mass [] = {4, 0, 7, 1};

        //Переворачиваем массив
        for (int i = 0; i < mass.length / 2; i++) {
            int tmp = mass[i];
            mass[i] = mass[mass.length - i - 1];
            mass[mass.length - i - 1] = tmp;
        }

        System.out.print("перевернутый массив: " + java.util.Arrays.toString(mass));

        for (int i = 0; i < mass.length ; i++) {
            System.out.print("[" + mass[i]+ "] ");
        }

        System.out.println();

        // Меняем первый и последний элементы местами

            int tmp = mass[0];
            mass[0] = mass[mass.length - 1];
            mass[mass.length - 1] = tmp;

        System.out.print ("Поменяли первый и последний элементы местами: ");
        for (int i = 0; i <mass.length ; i++) {
            System.out.print("[" + mass[i]+ "] ");
        }

        System.out.println();

        //Создадим новый массив в котором будут элементы первого массива + 3 новых элемента

       //Выделяем место для нового массива
        int newTestArray [] = new int[mass.length + 3];
            newTestArray [0] = 2;
            newTestArray [1] = 3;
            newTestArray [2] = 34;
            newTestArray [3] = 76;
            newTestArray [4] = 12;

        for (int i = 0; i < newTestArray .length ; i++) {
            System.out.print(" [ " + newTestArray [i] + " ] ");
        }

        //Записуем элементы старого массива в новый массив
        for (int i = 0; i < mass.length ; i++) {
            newTestArray [i] = mass[i];
        }
        System.out.println();

        //Выводим результат
        System.out.print("Результат старых элементов в новом массиве: ");

        for (int i = 0; i <newTestArray .length ; i++) {
            System.out.print(" [ " + newTestArray [i] + " ] ");
        }
        System.out.println();


        //Записываем новые элементы в оставшиеся ячейки
        for (int i = mass.length; i < newTestArray .length; i++) {
            if (newTestArray [i] == 0) {
                newTestArray [i] = i + 2;
            }
        }

        //Выводим конечный результат нового массива
        System.out.print("Конечный результат нового массива: ");
        for (int i = 0; i < newTestArray.length ; i++) {
            System.out.print(" [ " + newTestArray[i] + " ] ");
        }

        int [] arr = new int[10];

        //запись рандомных чисел от 0 до 10 в массив
        System.out.println("Массив рандомных чисел: ");
        for (int i = 0; i <arr.length ; i++) {
            arr[i] = (int)(Math.random() * (10 - 2)+1)+2;
            System.out.print(arr[i] + " , ");
        }
        System.out.println();
    }
}
