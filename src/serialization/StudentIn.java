package serialization;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class StudentIn {

    public static void main(String[] args) {
        try(ObjectInputStream in =
                        new ObjectInputStream(new FileInputStream("student.txt"))) {
            Student student = (Student)in.readObject();
            System.out.println(student);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
