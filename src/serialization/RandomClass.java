package serialization;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;

public class RandomClass implements Serializable {

    private static final long serialVersionUID = 1L;

    // Генерация рандомного значения
    private int countOfOrders[];
    private transient String password;
    private Address address;
    // Конструктор
    public RandomClass() {
        countOfOrders = new int[r()];
        for (int i = 0; i < countOfOrders.length; i++) {
            countOfOrders[i] = r();
        }
        password = "MyPassword";
        address = new Address("MyStreet");
    }
    private static int r() {
        return (int) (Math.random() * 10);
    }

    public void printOut() {
        System.out.println("This RandomClass has " + countOfOrders.length + " random integers");
        System.out.println(Arrays.toString(countOfOrders));
        System.out.println(address);
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        System.out.println("Пошла запись объекта");
        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        System.out.println("Reading....");
        in.defaultReadObject();
    }
}