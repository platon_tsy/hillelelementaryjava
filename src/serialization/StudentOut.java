package serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class StudentOut {

    public static void main(String[] args) {
        Student student = new Student("Vasya", 23);
        try (ObjectOutputStream outputStream =
                     new ObjectOutputStream(new FileOutputStream("student.txt"))) {
            outputStream.writeObject(student);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

}
