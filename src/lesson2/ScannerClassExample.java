package lesson2;

import java.util.Scanner;

public class ScannerClassExample {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter string: ");
        int integer = scanner.nextInt();
        System.out.println(++integer);
        //Get all string line
//        String str = scanner.nextLine();
//
//        System.out.println("String " +  str);
//
//        int value = Integer.parseInt(str);
//
//        System.out.println("value = " + ++value);
//
//        int value2 = Integer.parseInt(scanner.nextLine());
//
//        System.out.println("value2 = " + value2);
//
//        value =  ++value + value2 + value++;
//
//        System.out.println("Реальное значение : " + value);
//
//        String secondString = scanner.findInLine("Hello World").toUpperCase();
//
//        System.out.println(secondString);

//        Find only first word
//        String newScannerString = scanner.next();
//        System.out.println(newScannerString);

//        String input = "1 fish 2 fish red fish blue fish";
//
//        // \\s* means 0 or more repetitions of any whitespace character
//        // fish is the pattern to find
//        Scanner s = new Scanner(input).useDelimiter("\\s*fish\\s*");
//
//        System.out.println(s.nextInt());   // prints: 1
//        System.out.println(s.nextInt());   // prints: 2
//        System.out.println(s.next());      // prints: red
//        System.out.println(s.next());      // prints: blue
//
//        // don't forget to close the scanner!!
//        scanner.close();
//        s.close();

    }
}
