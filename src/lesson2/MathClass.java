package lesson2;


public class MathClass {

    public static void main(String[] args) {
        int x = 5;
        int y = 10;
        double q = -132;
        double c = 123.424343;
        System.out.println("Выводим 5 в степени 10: " + (int)Math.pow(x, y));
        // В Java Math.max() возвращает максимальное значение двух чисел.
        System.out.println(Math.max(x, y));
        // В Java Math.abs() возвращает абсолютное значение аргумента (модуль числа).
        System.out.println(Math.abs(x));
        System.out.println(Math.abs(q));
        System.out.println(Math.min(x, y));
        System.out.println(Math.round(c));
        System.out.println("converted = " + (int)c);
        System.out.println("!" + (int) Math.sqrt(x));
        // byte - short - int - long
        // float - double
        System.out.println(Math.sqrt(9));
        System.out.println(Math.random());

        if (Math.max(y, x) == 10) {
            System.out.println("y greater!");
        }
        int result;
        result = (int)(Math.random() * (y - x) +1 ) + x;
        System.out.println(" result = " + result);
        System.out.println(Math.PI);

    }
}
