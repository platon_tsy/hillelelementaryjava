package annotation;

@PermissionRequired(value = User.Permission.USER_MANAGEMENT)
public class UserDeleteAction implements UserAction {
    public void invoke(User user) {
        System.out.println(user.getName() + " - removed!");
    }
}