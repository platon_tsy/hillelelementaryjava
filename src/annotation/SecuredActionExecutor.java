package annotation;

import java.lang.reflect.Method;

public class SecuredActionExecutor {

    public static void execute(User user, UserAction action) throws Exception {
        Class<? extends UserAction> userActionClass = action.getClass();
        PermissionRequired permissionRequired = userActionClass.getAnnotation(PermissionRequired.class);
        if (permissionRequired != null){
            if (user != null && user.getPermissions().contains(permissionRequired.value())){
                Method invoke = userActionClass.getMethod("invoke", User.class);
                invoke.invoke(action, user);
            } else {
                System.err.println("User - " + user.getName() + " doesn't have required permission = " + permissionRequired.value());
            }
        }
    }

}
