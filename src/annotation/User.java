package annotation;

import java.util.ArrayList;
import java.util.List;

public class User {


    private String name;

    private List<Permission> permissions;

    public enum Permission {
        USER_MANAGEMENT, CONTENT_MANAGEMENT, ADMIN

    }

    public User() {
        permissions = new ArrayList<>();
    }

    @MyAnnotation(value = "Test", myint = 2)
    public List<Permission> getPermissions() {
        return new ArrayList<Permission>(permissions);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
}
