package annotation;


import java.util.Arrays;

public class Test {

    public static void main(String[] args) throws Exception {
        User fedir = new User();
        fedir.setName("Fedir");
        fedir.setPermissions(Arrays.asList(User.Permission.USER_MANAGEMENT));

        User petro = new User();
        petro.setName("Petro");
        petro.setPermissions(Arrays.asList(User.Permission.CONTENT_MANAGEMENT));

        User vasyl = new User();
        vasyl.setName("Vasyl");

        UserAction userDeleteAction = new UserDeleteAction();

        SecuredActionExecutor.execute(fedir, userDeleteAction);
        SecuredActionExecutor.execute(petro, userDeleteAction);
        SecuredActionExecutor.execute(vasyl, userDeleteAction);

    }

}
