package annotation;

public interface UserAction {
    public void invoke(User user);
}
