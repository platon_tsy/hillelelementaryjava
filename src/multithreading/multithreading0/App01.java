package multithreading.multithreading0;

public class App01 {

	public static void main(String[] args){
		
		Thread myThready = new Thread(new Runnable(){
			
			public void run() {
				System.out.println("Привет из побочного потока!");
			}
		});
		
		myThready.start();	

		System.out.println("Главный поток завершён...");
	}
}
