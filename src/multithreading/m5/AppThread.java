package multithreading.m5;

class AppThread extends Thread {

	public void run() {
		Thread ct = Thread.currentThread();
		System.out.println("Дочернийпоток - " + ct.getName());
		for (int i = 1; i <= 5; i++) {
			System.out.println("Значение цикла дочернего потока " + ct.getName() + " - " + i);
		}
		System.out.println("Работа дочернего потока завершена - " + ct.getName());
	}
}
