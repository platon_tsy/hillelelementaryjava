package multithreading.m5;

public class ThreadTest implements Runnable {
	private static ThreadTest shared = new ThreadTest();

	public void run() {
		shared.process();
	}
	
	public void process() {
		for (int i = 0; i < 3; i++) {
			System.out.println(Thread.currentThread().getName() + " " + i);
			Thread.yield();
		}
	}

	public static void main(String s[]) {
		for (int i = 0; i < 3; i++) {
			new Thread(new ThreadTest(), "Thread-" + i).start();
		}
	}
}
