package multithreading.m3;

public class DaemonThreadExample extends Thread{

	   public void run(){  
			
		  if(Thread.currentThread().isDaemon()){ 
		      System.out.println("Daemon thread executing");  
		  }  
		  else{  
		      System.out.println("user(normal) thread executing");  
	      }  
	   }  
	  
}
