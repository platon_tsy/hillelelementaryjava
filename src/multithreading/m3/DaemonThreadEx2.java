package multithreading.m3;

public class DaemonThreadEx2 extends Thread {
	
	public void run(){  
		  try {
			System.out.println("Thread is running");
			Thread.sleep(5000L);
			System.out.println("Thread after sleeping");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		     
	   } 
}
