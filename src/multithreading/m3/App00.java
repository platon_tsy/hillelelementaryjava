package multithreading.m3;

public class App00 {
	public static void main(String[] args) {

		DaemonThreadExample t1 = new DaemonThreadExample();
		DaemonThreadExample t2 = new DaemonThreadExample();

		t1.setDaemon(true);

		t1.start();
		t2.start();
	}
}
