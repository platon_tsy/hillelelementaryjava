package multithreading.m4;

public class Cat implements Runnable {
	private boolean isInterrupt;
    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted() && !isInterrupt){
            System.out.println("Сон начинается");
            sleep2sec();
            System.out.println("Сон закончился\n");
        }
        System.out.println("Завершение работы потока");
    }
     
    private void sleep2sec() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            System.out.println("Сон прерван");
            isInterrupt = true;
        }
    }
}
