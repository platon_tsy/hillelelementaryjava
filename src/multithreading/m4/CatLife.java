package multithreading.m4;

public class CatLife {
	
	public static void main(String[] args) throws InterruptedException {
		
		Thread cat = new Thread(new Cat());
		cat.start();
		Thread.sleep(5000);
		cat.interrupt();
		cat.join();
	}
}
