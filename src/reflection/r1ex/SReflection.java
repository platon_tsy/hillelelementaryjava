package reflection.r1ex;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class SReflection
{
    public static void main(String[] args) {
        try {
            demoReflection();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
 
    private static void demoReflection() throws Exception {
        Class example = Class.forName("multithreading2.r1ex.SClass");
        SClass sc = (SClass) example.newInstance();
 
        demoReflectionField(example, sc);
        
        demoReflectionMethod(example, sc);
    }
 
    private static void demoReflectionField(Class example, SClass sc) throws Exception {
        Field f = example.getDeclaredField("first");
        f.setAccessible(true);
        String test = (String)f.get(sc);
        System.out.println("Field before SET:" + sc.getFirst());
        f.set(sc, "Test");
        System.out.println("Field after SET:" + sc.getFirst());
    }
 
    private static void demoReflectionMethod(Class example, SClass sc) throws Exception {
        Method method1 = example.getMethod("simple");
        String simple = (String)method1.invoke(sc);
        System.out.println("Simple:" + simple);
        
        // Вызов метода с параметрами
        // Сначала надо определить список параметров - вспоминаем overloading
        // У нас это две строки - String
        Class[] paramTypes = new Class[] {String.class, String.class};
        // Получить обхект типа Method по имени и по списку параметров
        Method concat = example.getMethod("concat", paramTypes);
        // Вызвать метод - передать туда объект и два параметра типа строка
        String answer = (String)concat.invoke(sc, "1", "2");
        System.out.println("Concat:" + answer);
    }
}
