package socket;

public class MainMulti {

    public static void main(String[] args) {
        EchoClient client1 = new EchoClient();
        client1.startConnection("127.0.0.1", 5555);
        String msg1 = client1.sendMessage("hello");
        String msg2 = client1.sendMessage("world");
        String terminate = client1.sendMessage(".");
        System.out.println(msg1);
        System.out.println(msg2);


        EchoClient client2 = new EchoClient();
        client2.startConnection("127.0.0.1", 5555);
        String msg3 = client2.sendMessage("hello2");
        String msg4 = client2.sendMessage("world2");

        String terminate2 = client1.sendMessage(".");
        System.out.println(msg3);
        System.out.println(msg4);
    }

}
